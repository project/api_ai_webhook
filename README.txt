CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* General Configuration
* Entities Push
* Maintainers


INTRODUCTION
------------

This module integrates your Drupal website with Dialogflow[1] (ex Api.AI)
Webooks, letting your Dialogflow agents talk with your website, respond
to Intents request and fill slots.

Version 2.x (work in progress) is compatible only with Dialogflow V2.


REQUIREMENTS
------------

Dialogflow Webhook requires 3rd party PHP libraries.
If you have a composer-based project you are covered, otherwise you'll need
download and autoload it manually. Check "Installing modules' Composer
dependencies"[3] and also "Using Composer to manage Drupal site dependencies"[4]
for some approaches on how to handle this.


RECOMMENDED MODULES
-------------------

Developers and Site builder may want to use Chatbot API module and take advantage
of its Intents pluggable system in order to simplify the implementation of their
Intents logic, as well as its chatbot_api_entities submodule to dynamically push
Drupal content to Dialogflow entities.


INSTALLATION
------------

Install the module as you would normally install a contributed Drupal
module.

Configuring the module is optional, although suggested if your webhook handles data requiring protection. If no
authentication method your webhook(s) will be publicly accessible.


GENERAL CONFIGURATION
---------------------

    1. Navigate to Administration > Configuration > Web Services > Dialogflow Webhook Webhook Configuration.
    2. Configure the preferred authentication method.

The settings are not stored in the configuration, for security reason. They
are instead stored in the states. That means if you deploy an import the
config, Dialogflow settings are not deployed and you need to move them across
environment in another way, or re-submit the form in your final enviornment(s).

You are now ready to receive POST from Dialogflow. In the agent webhook
configuration use the URL:

  https://your-website.com/api.ai/webhook

Please note although it's not mentioned anywhere, it looks like HTTP over SSL
(HTTPS) seems required. Using and HTTP only URL can fail silently.


ENTITIES PUSH
-------------
The module provides the ability to synchronise your entities in Drupal in entities
on Dialogflow. See "Sending Entities to remote services"[6] documentation page for
more info.

In order to be able to authorise your Drupal site to push entities to Dialogflow
agent, you need to following these step to grant permissions.

1. Create a service account on GCP Console [7]
2. Save the .json file locally (or wherever your Drupal instance is running)
3. Create a `GOOGLE_APPLICATION_CREDENTIALS` environment variable with variable the path to your credentials .json file
4. Check the Status - Report page, on "Dialogflow Webhook - Entity Push Status " section, to see if client library and
credentials environment variables are setup correctly.

Worth mentioning the module only check the existence of the variables, not actually its content. So make sure you store
the .json file as exported from the google console, without manual alteration.

Also, make sure GOOGLE_APPLICATION_CREDENTIALS will not be lost on rebooting your server. Depending by your setup
this can be achieved in several ways, which our out of the scope of this module and its documentation.

Store the .json file in a safe place, only accessible by authorised users. For example do NOT store it
anywhere inside the Drupal docroot!

If you need any more details, check out this link: https://developers.google.com/accounts/docs/application-default-credentials


MAINTAINERS
-----------

The 8.x-1.x and 8.x-2.x branches were created by:

 * Gabriele Maira (gambry) - https://www.drupal.org/u/gambry

Additional work and new features have been introduced with the great work
done by:

 * Lee Rowlands (larowlan) - https://www.drupal.org/u/larowlan

Sponsored by Manifesto[5]


LINKS
-----

[1] https://dialogflow.com/
[3] https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies
[4] https://www.drupal.org/node/2718229
[5] https://manifesto.co.uk
[6] https://www.drupal.org/docs/8/modules/chatbot-api/sending-entities-to-remote-services
[7] https://cloud.google.com/docs/authentication/production#creating_a_service_account
