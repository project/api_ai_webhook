<?php

namespace Drupal\Tests\api_ai_webhook\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\api_ai_webhook\Plugin\ChatbotApiEntities\PushHandler\ApiAiPushHandler;
use Drupal\chatbot_api_entities\Entity\EntityCollectionInterface;
use Drupal\Tests\UnitTestCase;
use Google\ApiCore\PagedListResponse;
use Google\Cloud\Dialogflow\V2\EntityType;
use Google\Cloud\Dialogflow\V2\EntityTypesClient;
use Prophecy\Argument;
use Psr\Log\NullLogger;

/**
 * Tests api ai push handler.
 *
 * @group api_ai_webhook
 */
class ApiAiPushHandlerTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * Tests api handler saves.
   */
  public function testSaveConfiguration() {

    // Entity type existing on remote.
    $entity_type = new EntityType();
    $entity_type->setDisplayName('Foo');
    $entity_type->setName('projects/abc-123/agent/entityTypes/foo');

    // Client and paged response mock-up.
    $page = $this->prophesize(PagedListResponse::class);
    $page->iterateAllElements()->willReturn([$entity_type]);
    $client = $this->prophesize(EntityTypesClient::class);
    $client->listEntityTypes("projects/abc-123/agent")->willReturn($page);
    $client->close()->willReturn();

    $plugin_configuration = ['settings' => ['remote_name' => 'Foo', 'project_id' => 'abc-123']];
    $logger = new NullLogger();
    $handler = new ApiAiPushHandler($plugin_configuration, 'foo', [], $client->reveal(), $logger);

    // This is a new setup collection. Should trigger the client.
    $newCollection = $this->prophesize(EntityCollectionInterface::class);
    $newCollection->isNew()->willReturn(TRUE);
    $newCollection->label()->willReturn('Foo');

    // Should return an existing Foo entity, and not create new ones.
    $configuration = $handler->saveConfiguration($newCollection->reveal(), $plugin_configuration);
    $this->assertEquals('projects/abc-123/agent/entityTypes/foo', $configuration['settings']['remote_id']);
    // Assert client have been called.
    $client->listEntityTypes("projects/abc-123/agent")->shouldHaveBeenCalled();
    // Assert no new entities have been created.
    $client->createEntityType()->shouldNotHaveBeenCalled();

    // Should do nothing due not-new entity collection, and return untouched
    // configuration.
    $oldCollection = $this->prophesize(EntityCollectionInterface::class);
    $oldCollection->isNew()->willReturn(FALSE);
    $configuration = $handler->saveConfiguration($oldCollection->reveal(), ['do-not-process-me']);
    $this->assertEquals(['do-not-process-me'], $configuration);

    // Should create a new Foobar entity existing ID.
    $plugin_configuration['settings']['remote_name'] = 'Foobar';
    $new_entity_type = new EntityType();
    $new_entity_type->setDisplayName('Foo');
    $new_entity_type->setName('projects/abc-123/agent/entityTypes/foobar');
    $client->createEntityType("projects/abc-123/agent", Argument::type(EntityType::class))->willReturn($new_entity_type);
    $configuration = $handler->saveConfiguration($newCollection->reveal(), $plugin_configuration);
    $this->assertEquals('projects/abc-123/agent/entityTypes/foobar', $configuration['settings']['remote_id']);
    $client->createEntityType("projects/abc-123/agent", Argument::type(EntityType::class))->shouldHaveBeenCalledOnce();
  }

}
