<?php

namespace Drupal\Tests\api_ai_webhook\Unit;

use DialogFlow\Model\Webhook\Response;
use Drupal\chatbot_api_apiai\IntentResponseApiAiProxy;
use Drupal\Tests\UnitTestCase;

/**
 * Tests Dialogflow IntentResponseApiAiProxy class.
 *
 * @group api_ai_webhook
 */
class IntentResponseApiAiProxyTest extends UnitTestCase {

  public function testSetIntentResponseBackCompatibility() {
    $original_response = new Response();
    $response = new IntentResponseApiAiProxy($original_response);
    $response->setIntentResponse('My Text');
    $expected = [
      'fulfillmentText' => 'My Text',
      'fulfillmentMessages' => [['text' => ['text' => ['My Text']]]]
    ];
    $this->assertEquals($expected, $response->jsonSerialize());
  }

  /**
   * @dataProvider intentResponsesProvider
   */
  public function testSetIntentResponse($data, $type, $expected) {
    $original_response = new Response();
    $response = new IntentResponseApiAiProxy($original_response);
    $response->setIntentResponse($data, $type);
    $this->assertEquals($expected, $response->jsonSerialize());
  }

  public function intentResponsesProvider() {
    return [
      ['Whatever text', 'text', [
        'fulfillmentText' => 'Whatever text',
        'fulfillmentMessages' => [['text' => ['text' => ['Whatever text']]]]
      ]],
      [['name' => 'my-event'], 'event', [
        'followupEventInput' => ['name' => 'my-event']
      ]],
      [['title' => 'Card Title'], 'card', [
        'fulfillmentMessages' => [['card' => ['title' => 'Card Title']]]
      ]],
      [['slack' => ['text' => 'Slack message']], 'payload', [
        'fulfillmentMessages' => [['payload' => ['slack' => ['text' => 'Slack message']]]]
      ]]
    ];
  }
}
