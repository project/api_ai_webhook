<?php

/**
 * @file
 * Install, update and uninstall functions for the Dialogflow Webhook module.
 */

/**
 * Implements hook_uninstall().
 */
function api_ai_webhook_uninstall() {
  // Delete data from state storage.
  Drupal::state()->delete('api_ai_webhook.auth');
}

/**
 * Implements hook_requirements().
 */
function api_ai_webhook_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    // Check is entity push can be used. Possible status:
    // -1   Google library missing.
    // 0    Library present, credentials env variable missing or wrong.
    // 1    Library present, env variable present.
    $value = REQUIREMENT_INFO;
    $status = t('Entity push not enabled (library missing).');
    if (class_exists('Google\Cloud\Dialogflow\V2\EntityTypesClient')) {
      $value = REQUIREMENT_WARNING;
      $credentials = getenv('GOOGLE_APPLICATION_CREDENTIALS');
      $status = t('Variable missing or not correctly configured. Entity Push feature may not work. In order to hide this warning either remove google/cloud-dialogflow library - if you do not need Enttiy Push - OR correctly setup the GOOGLE_APPLICATION_CREDENTIALS variable.');
      if ($credentials && file_exists($credentials)) {
        $value = REQUIREMENT_OK;
        $status = 'Entity Push library and credentials variable found.';
      }
    }

    $requirements['api_ai_webhook_status'] = [
      'title' => t('Dialogflow Webhook - Entity Push Status'),
      'value' => $status,
      'severity' => $value,
    ];
  }

  return $requirements;
}
