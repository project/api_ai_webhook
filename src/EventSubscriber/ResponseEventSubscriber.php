<?php

namespace Drupal\api_ai_webhook\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Subscribe to the response in order to better handle Dialogflow errors.
 *
 * @package Drupal\api_ai_webhook
 */
class ResponseEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events['kernel.response'][] = ['alterResponse'];

    return $events;
  }

  /**
   * Alter the response if is an Dialogflow webhook error.
   *
   * This method is called whenever the kernel.response event is dispatched,
   * then we filter responses/requests coming form Dialogflow webhook.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The repose event.
   */
  public function alterResponse(ResponseEvent $event) {
    if (rtrim($event->getRequest()->getPathInfo(), '/') === '/api.ai/webhook') {

      // Handle errors.
      if ($event->getResponse()->isClientError() || $event->getResponse()->isServerError()) {
        $data = [
          'status' => [
            'code' => $event->getResponse()->getStatusCode(),
            'errorType' => JsonResponse::$statusTexts[$event->getResponse()->getStatusCode()],
          ],
          'speech' => 'An error occurred.',
        ];

        $event->setResponse(new JsonResponse($data, $event->getResponse()->getStatusCode()));
      }
    }
  }

}
