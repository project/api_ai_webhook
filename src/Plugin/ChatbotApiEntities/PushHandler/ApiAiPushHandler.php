<?php

namespace Drupal\api_ai_webhook\Plugin\ChatbotApiEntities\PushHandler;

use Drupal\chatbot_api_entities\Entity\EntityCollection;
use Drupal\chatbot_api_entities\Entity\EntityCollectionInterface;
use Drupal\chatbot_api_entities\Plugin\PushHandlerInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Error;
use Google\ApiCore\ApiException;
use Google\Cloud\Dialogflow\V2\EntityType;
use Google\Cloud\Dialogflow\V2\EntityType\Entity;
use Google\Cloud\Dialogflow\V2\EntityType\Kind;
use Google\Cloud\Dialogflow\V2\EntityTypesClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a handler for pushing entities to Dialogflow API.
 *
 * @PushHandler(
 *   id = "api_ai_webhook",
 *   label = @Translation("Dialogflow entities endpoint")
 * )
 */
class ApiAiPushHandler extends PluginBase implements PushHandlerInterface, ContainerFactoryPluginInterface {

  /**
   * Site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Google entity type handler client.
   *
   * @var \Google\Cloud\Dialogflow\V2\EntityTypesClient
   */
  protected $client;

  /**
   * Constructs a new ApiAiPushHandler object.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Google\Cloud\Dialogflow\V2\EntityTypesClient $client
   *   Google Entity Types handler client.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypesClient $client, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
    $this->logger = $logger;
    $this->configuration += [
      'settings' => [
        'remote_name' => '',
        'remote_id' => '',
        'project_id' => '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      new EntityTypesClient(),
      $container->get('logger.factory')->get('api_ai_webhook')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function pushEntities(array $entities, EntityCollection $entityCollection) {
    $entries = $this->formatEntries($entities, $entityCollection);
    $entityTypeId = $this->configuration['settings']['remote_id'];
    $loggerArgs = [
      '@name' => $entityCollection->label(),
    ];
    try {
      $this->client->batchCreateEntities($entityTypeId, $entries);
      $this->logger->info('Synchronized Chatbot API entity collection @name to Dialogflow', $loggerArgs);
    }
    catch (ApiException $e) {
      $this->logger->error('Error synchronizing Chatbot API entity collection @name to Dialogflow - %type: @message in %function (line %line of %file)', Error::decodeException($e) + $loggerArgs);
    }
    finally {
      $this->client->close();
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatEntries(array $entities, EntityCollection $entityCollection) {
    $formatted = [];
    foreach ($entities as $entity) {
      $remote_entity = new Entity();
      $remote_entity->setValue($entity->label());
      $remote_entity->setSynonyms($entityCollection->getSynonyms($entity));
      $formatted[] = $remote_entity;
    }
    return $formatted;
  }

  /**
   * {@inheritdoc}
   */
  public function saveConfiguration(EntityCollectionInterface $entityCollection, array $configuration) {
    if (!empty($configuration['settings']['remote_id'])) {
      return $configuration;
    }
    if ($entityCollection->isNew()) {

      // Merge provided configuration on top of instance level values.
      $configuration = $configuration + $this->configuration;

      $loggerArgs = [
        '@name' => $entityCollection->label(),
      ];
      $existing = $this->getExistingEntityMap($configuration['settings']['project_id'], $loggerArgs);
      $remote_name = strtolower($configuration['settings']['remote_name']);
      if (isset($existing[$remote_name])) {
        $configuration['settings']['remote_id'] = $existing[$remote_name];
        return $configuration;
      }
      // New entity, we need to push.
      try {
        $parent = EntityTypesClient::projectAgentName($configuration['settings']['project_id']);
        $entityType = new EntityType();
        $entityType->setDisplayName($remote_name);
        $entityType->setKind(Kind::KIND_MAP);
        $response = $this->client->createEntityType($parent, $entityType);
        $this->logger->info('Generated remote ID for Chatbot API entity collection @name on Dialogflow', $loggerArgs);
        $configuration['settings']['remote_id'] = $response->getName();
      }
      catch (ApiException $e) {
        $this->logger->error('Error generate remote ID for Chatbot API entity collection @name on Dialogflow - %type: @message in %function (line %line of %file)', Error::decodeException($e) + $loggerArgs);
        throw new EntityStorageException(sprintf('Unable to generate a remote ID for entity collection %s.', $entityCollection->label()));
      }
      finally {
        $this->client->close();
      }
    }
    return $configuration;
  }

  /**
   * Gets map of existing entity type IDs keyed by lowercase name.
   *
   * @param string $project_id
   *   Dialogflow project id.
   * @param array $loggerArgs
   *   Array of variables replacements for logger.
   *
   * @return array
   *   Array with entity name as key and ID as value.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If cannot communicate with the remote end point.
   * @throws \Google\ApiCore\ValidationException
   */
  protected function getExistingEntityMap($project_id, array $loggerArgs) {
    try {
      $parent = EntityTypesClient::projectAgentName($project_id);
      $entityTypes = $this->client->listEntityTypes($parent);
      $existing_entities = [];
      foreach ($entityTypes->iterateAllElements() as $entityType) {
        $existing_entities[strtolower($entityType->getDisplayName())] = $entityType->getName();
      }
      return $existing_entities;
    }
    catch (ApiException $e) {
      $this->logger->error('Error getting list of existing remote IDs for Chatbot API entity collection @name on Dialogflow - %type: @message in %function (line %line of %file)', Error::decodeException($e) + $loggerArgs);
      throw new EntityStorageException(sprintf('Error getting list of existing remote IDs for Chatbot API entity collection %s on Dialogflow', $loggerArgs['@name']));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(EntityCollectionInterface $entityCollection, array $form, FormStateInterface $form_state) {
    return [
      'project_id' => [
        '#type' => 'textfield',
        '#title' => new TranslatableMarkup('Project Id'),
        '#description' => new TranslatableMarkup('Your Dialogflow agent Project Id.'),
        '#default_value' => $this->configuration['settings']['project_id'],
      ],
      'remote_name' => [
        '#type' => 'textfield',
        '#title' => new TranslatableMarkup('Remote name'),
        '#description' => new TranslatableMarkup('Give the collection a name on Dialogflow'),
        '#default_value' => $this->configuration['settings']['remote_name'],
      ],
      'remote_id' => [
        '#type' => 'value',
        '#value' => $this->configuration['settings']['remote_id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettingsForm(EntityCollectionInterface $entityCollection, array $form, FormStateInterface $form_state) {
    if (!$form_state->getValue([
      'push_handler_configuration',
      $this->pluginId,
      'settings',
      'remote_name',
    ])) {
      $form_state->setError($form['push_handlers']['settings'][$this->pluginId], new TranslatableMarkup('Remote name is required'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return !empty($this->configuration['status']);
  }

}
