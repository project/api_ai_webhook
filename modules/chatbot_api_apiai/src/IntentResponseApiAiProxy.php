<?php

namespace Drupal\chatbot_api_apiai;

use DialogFlow\Model\Webhook\Response;
use Drupal\chatbot_api\IntentResponseInterface;

/**
 * Proxy wrapping Dialogflow Response in a ChatbotRequestInterface.
 *
 * @package Drupal\chatbot_api_apiai
 */
class IntentResponseApiAiProxy implements IntentResponseInterface {

  use ApiAiContextTrait;

  /**
   * Original object.
   *
   * @var \DialogFlow\Model\Webhook\Response
   */
  protected Response $original;

  /**
   * IntentResponseAlexaProxy constructor.
   *
   * @param \DialogFlow\Model\Webhook\Response $original
   *   Original response instance.
   */
  public function __construct(Response $original) {
    $this->original = $original;
  }

  /**
   * Proxy-er calling original response methods.
   *
   * @param string $method
   *   The name of the method being called.
   * @param array $args
   *   Array of arguments passed to the method.
   *
   * @return mixed
   *   Value returned from the method.
   */
  public function __call($method, array $args) {
    return call_user_func_array([$this->original, $method], $args);
  }

  /**
   * {@inheritdoc}
   */
  public function addIntentAttribute($name, $value, $lifespan = 5) {

    // Lookup for existing context.
    $contexts = $this->original->get('outputContexts', []);
    /** @var \DialogFlow\Model\Context $context */
    foreach ($contexts as $context) {
      if ($this->contextNameIs($context, $this->getContextName($name))) {
        // Original library doesn't allow parameters settings. Let's work
        // this around.
        $params = $context->getParameters();
        $params[$this->getParameterName($name)] = $value;
        $context->add('parameters', $params);
        return;
      }
    }

    // No context with this name has been found. Create a new one.
    $data = [
      'name' => $this->getContextName($name),
      'lifespanCount' => $lifespan,
      'parameters' => [
        $this->getParameterName($name) => $value,
      ],
    ];
    $context = $this->original->createContextFromSession($data['name'], $data);
    $this->original->addContext($context);
  }

  /**
   * Set intent response.
   *
   * @param mixed $data
   *   The data to set as fulfilment response. Its content differs depending on
   *   the $type parameter.
   * @param string $type
   *   The data type to add to the fulfillment. It can be either 'text', 'card',
   *   'payload' or 'event'. The default is 'text' for back-compatibility.
   *
   * @return self
   *   The current IntentResponseInterface instance.
   */
  public function setIntentResponse(mixed $data, string $type = 'text') {
    switch ($type) {
      case 'card':
      case 'payload':
        $this->original->getFulfillment()->addMessage($data, $type);
        break;
      case 'event':
        $this->original->getFulfillment()->setEvent($data);
        break;
      case 'text':
      default:
        $this->original->getFulfillment()->setText($data);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setIntentDisplayCard($content, $title = NULL) {
    // This feature has been removed from Dialogflow.
    // @see \DialogFlow\Model\Webhook\Response::setDisplayText()
    return $this;
  }

}
