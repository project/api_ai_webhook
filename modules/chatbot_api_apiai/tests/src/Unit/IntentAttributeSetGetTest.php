<?php

namespace Drupal\Tests\chatbot_api_apiai\Unit;

use DialogFlow\Model\Webhook\Request;
use DialogFlow\Model\Webhook\Response;
use Drupal\chatbot_api_apiai\IntentRequestApiAiProxy;
use Drupal\chatbot_api_apiai\IntentResponseApiAiProxy;
use Drupal\Tests\UnitTestCase;

/**
 * Tests set/get intent attributes for Dialogflow proxy classes.
 *
 * @group api_ai_webhook
 */
class IntentAttributeSetGetTest extends UnitTestCase {

  /**
   * Tests getIntentAttribute() method.
   *
   * @dataProvider requestJson
   */
  public function testGetIntentAttribute($json) {
    $request_data = json_decode($json, TRUE);

    // Add to the base JSON the contexts needed for this test.
    $context_prefix = str_replace('generic-context-name', '', $request_data['queryResult']['outputContexts'][0]['name']);
    $request_data['queryResult']['outputContexts'] = [
      [
        'name' => $context_prefix . 'weather',
        'lifespanCount' => 2,
        'parameters' => [
          'city' => 'Rome',
          'day' => 'Monday',
        ],
      ],
      [
        'name' => $context_prefix . 'persona',
        'lifespanCount' => 2,
        'parameters' => [
          'name' => 'Marie',
          'gender' => 'female',
        ],
      ],
      [
        'name' => $context_prefix . 'mycontextname',
        'lifespanCount' => 2,
        'parameters' => [
          'value' => 'hello world',
        ],
      ],
    ];

    $original_request = new Request($request_data);
    $request = new IntentRequestApiAiProxy($original_request);

    $this->assertEquals('Rome', $request->getIntentAttribute('weather.city'));
    $this->assertEquals('Monday', $request->getIntentAttribute('weather.day'));
    $this->assertEquals('Marie', $request->getIntentAttribute('persona.name'));
    $this->assertEquals('female', $request->getIntentAttribute('persona.gender'));

    // Make sure accessing a context by name is case-insensitive.
    $this->assertEquals('hello world', $request->getIntentAttribute('MyContextName.value'));
  }

  /**
   * Tests setIntentAttribute() method.
   */
  public function testSetIntentAttribute() {

    $fake_session = 'projects/your-agents-project-id/agent/sessions/88d13aa8-2999-4f71-b233-39cbf3a824a0';
    $original_response = new Response([], $fake_session);
    $response = new IntentResponseApiAiProxy($original_response);

    // Set some contexts.
    $response->addIntentAttribute('weather.city', 'Rome');
    $response->addIntentAttribute('weather.day', 'Monday');
    $response->addIntentAttribute('persona.name', 'Marie');
    $response->addIntentAttribute('persona.gender', 'female');
    $likes = ['sea', 'food', 'drupal'];
    $response->addIntentAttribute('persona.likes', $likes);

    // Make sure context name get/set is case insensitive.
    $response->addIntentAttribute('MyContextName.key', 'foo');
    $response->addIntentAttribute('MyContextName.value', 'bar');
    $response->addIntentAttribute('mycontextname.full', 'foo bar');

    // Assert setter works.
    $data = $response->jsonSerialize();
    $this->assertArrayHasKey('outputContexts', $data);
    $this->assertEquals($data['outputContexts'][0]->getName(), 'weather');
    $this->assertEquals($data['outputContexts'][0]->getParameters()['city'], 'Rome');
    $this->assertEquals($data['outputContexts'][0]->getParameters()['day'], 'Monday');
    $this->assertEquals($data['outputContexts'][1]->getName(), 'persona');
    $this->assertEquals($data['outputContexts'][1]->getParameters()['name'], 'Marie');
    $this->assertEquals($data['outputContexts'][1]->getParameters()['gender'], 'female');
    $this->assertEquals($data['outputContexts'][1]->getParameters()['likes'], $likes);

    // Change some parameters. Change the value type too, to make sure
    // overriding the value type is allowed.
    $response->addIntentAttribute('weather.day', ['Monday', 'Thursday']);
    $response->addIntentAttribute('persona.likes', 'all the small things');

    // Assert setter works with changing existing values and their types.
    $data = $response->jsonSerialize();
    $this->assertEquals($data['outputContexts'][0]->getParameters()['day'], ['Monday', 'Thursday']);
    $this->assertEquals($data['outputContexts'][1]->getParameters()['likes'], 'all the small things');

    // Also make sure previous parameters are unaltered.
    $this->assertEquals($data['outputContexts'][0]->getParameters()['city'], 'Rome');
    $this->assertEquals($data['outputContexts'][1]->getParameters()['gender'], 'female');

    // Make sure the case-sensitivity of the context names is respected on
    // set().
    $this->assertEquals($data['outputContexts'][2]->getName(), 'MyContextName');
    $this->assertEquals($data['outputContexts'][2]->getParameters()['key'], 'foo');
    $this->assertEquals($data['outputContexts'][2]->getParameters()['value'], 'bar');
    $this->assertEquals($data['outputContexts'][2]->getParameters()['full'], 'foo bar');
  }

  /**
   * Request JSON data provider.
   *
   * @return string[]
   *   Array of JSON data.
   */
  public function requestJson() {
    // V2 JSON example, extended from:
    // https://raw.githubusercontent.com/dialogflow/fulfillment-webhook-json/master/requests/v2/request.json
    $body = <<<EOL
{
  "responseId": "7811ac58-5bd5-4e44-8d06-6cd8c67f5406",
  "session": "projects/your-agents-project-id/agent/sessions/88d13aa8-2999-4f71-b233-39cbf3a824a0",
  "queryResult": {
    "queryText": "user's original query to your agent",
    "parameters": {
      "param1": "foo",
      "param2": "bar"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentText": "Text defined in Dialogflow's console for the intent that was matched",
    "fulfillmentMessages": [
      {
        "text": {
          "text": [
            "Text defined in Dialogflow's console for the intent that was matched"
          ]
        }
      }
    ],
    "outputContexts": [
      {
        "name": "projects/your-agents-project-id/agent/sessions/88d13aa8-2999-4f71-b233-39cbf3a824a0/contexts/generic-context-name",
        "lifespanCount": 5,
        "parameters": {
          "param1": "foo",
          "param2": "bar"
        }
      }
    ],
    "intent": {
      "name": "projects/your-agents-project-id/agent/intents/29bcd7f8-f717-4261-a8fd-2d3e451b8af8",
      "displayName": "Name of Matched Dialogflow Intent",
      "webhookState": 2
    },
    "intentDetectionConfidence": 1,
    "diagnosticInfo": {},
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {}
}
EOL;
    return [[$body]];
  }

}
